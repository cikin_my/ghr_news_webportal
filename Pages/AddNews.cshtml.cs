using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System.IO;
using Microsoft.AspNetCore.Http;
using System.Net;
using FileUpload.Utilities;
using Microsoft.AspNetCore.Authorization;

namespace News_WebPortal.Pages
{
    [Authorize]
    public class AddNewsModel : PageModel
    {
        
        public void OnGet()
        {

        }
        [ValidateAntiForgeryToken]
        public async Task OnPostAsync(List<IFormFile> files)
        {
            foreach (var file in files)
            {
                //get uploaded file name: true to create temp name, false to get real name
                var fileName = file.TempFileName(false);

                if (file.Length > 0)
                {
                    // optional : server side resize create image with watermark
                    // these steps requires LazZiya.ImageResize package from nuget.org

                    // upload and save files to upload folder
                    using (var stream = new FileStream($"wwwroot\\uploads\\{fileName}", FileMode.Create))
                    {
                        await file.CopyToAsync(stream);
                    }

                }
            }
        }

        public JsonResult OnGetListFolderContents()
        {
            var folderPath = $"wwwroot\\uploads";

            if (!Directory.Exists(folderPath))
                return new JsonResult("Folder not exists!") { StatusCode = (int)HttpStatusCode.NotFound };

            var folderItems = Directory.GetFiles(folderPath);

            if (folderItems.Length == 0)
                return new JsonResult("Folder is empty!") { StatusCode = (int)HttpStatusCode.NoContent };

            var galleryItems = new List<FileItem>();

            foreach (var file in folderItems)
            {
                var fileInfo = new FileInfo(file);
                galleryItems.Add(new FileItem
                {
                    Name = fileInfo.Name,
                    FilePath = $"uploads/{fileInfo.Name}",
                    FileSize = fileInfo.Length
                });
            }

            return new JsonResult(galleryItems) { StatusCode = 200 };
        }

        public JsonResult OnGetDeleteFile(string file)
        {
            var filePath = Path.Combine($"wwwroot\\uploads\\{file}");

            try
            {
                System.IO.File.Delete(filePath);
            }
            catch
            {
                return new JsonResult(false) { StatusCode = (int)HttpStatusCode.InternalServerError };
            }

            return new JsonResult(true) { StatusCode = (int)HttpStatusCode.OK };
        }
    }
}
