(function ($) {
    'use strict';
    $(function () {
        var body = $('body');
        var contentWrapper = $('.content-wrapper');
        var scroller = $('.container-scroller');
        var footer = $('.footer');
        var sidebar = $('.sidebar');

        //Add active class to nav-link based on url dynamically
        //Active class can be hard coded directly in html file also as required

        function addActiveClass(element) {

          if (current === "") {

            //for root url
            if (element.attr('href').indexOf("index") !== -1) {
              element.parents('.nav-item').last().addClass('active');
              if (element.parents('.sub-menu').length) {
                element.closest('.collapse').addClass('show');
                element.addClass('active');

              }
            }
          } else {

            //for other url
            if (element.attr('href').indexOf(current) !== -1) {
              element.parents('.nav-item').last().addClass('active');
              if (element.parents('.sub-menu').length) {
                element.closest('.collapse').addClass('show');
                element.addClass('active');
              }
              if (element.parents('.submenu-item').length) {
                element.addClass('active');

              }
            }
          }
        }

        var current = location.pathname.split("/").slice(-1)[0].replace(/^\/|\/$/g, '');
        $('.nav li a', sidebar).each(function() {
          var $this = $(this);   
          addActiveClass($this);
        })


        // Get the last item in the path (e.g. index.php)
        let url = window.location.pathname.split('/').pop();

        // Add active nav class based on url
        $("#sidebar ul li .nav-item").each(function () {
          if ($(this).attr("href") == url || $(this).attr("href") == '') {
            $(this).closest('li').addClass("active");
          }
        })

        //Add the active class into Home if user omit index.php from url
        if (url == '') {
          $("#sidebar ul li").eq(0).addClass("active");
        }
        // =========
        // $(function () {
        //     setNavigation();
        // });

        // function setNavigation() {
        //     var path = window.location.pathname;
        //     path = path.replace(/\/$/, "");
        //     path = decodeURIComponent(path);

        //     $(".nav a").each(function () {
        //         var href = $(this).attr('href');
        //         console.log('href: ' + href);
        //         if (path.substring(0, href.length) === href) {
        //             $(this).closest('li').addClass('active');
        //         }
        //     });
        // }

        //Close other submenu in sidebar on opening any

        sidebar.on('show.bs.collapse', '.collapse', function () {
            sidebar.find('.collapse.show').collapse('hide');
        });


        //Change sidebar 
        $('[data-toggle="minimize"]').on("click", function () {
            body.toggleClass('sidebar-icon-only');
        });

        //checkbox and radios
        $(".form-check label,.form-radio label").append('<i class="input-helper"></i>');

    });

    // focus input when clicking on search icon
    $('#navbar-search-icon').click(function () {
        $("#navbar-search-input").focus();
    });

})(jQuery);